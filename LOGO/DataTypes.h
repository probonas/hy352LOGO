#pragma once
#include "pch.h"

#include <string>
#include <iostream>
#include <cassert>
#include <vector>

namespace logo {
//#define DEBUG

#ifdef DEBUG
#define TRACE_INFO(X)			std::cout << X << std::endl;
#else
#define TRACE_INFO(X)			;
#endif // DEBUG

	enum DataTypes { number, word, boolean, sentence, list, array, list_with_single_value };

	class ListEntry;

	class LogoType {

	protected:
		DataTypes type;

	public:
		DataTypes getType() {
			return type;
		}

		virtual void setType(DataTypes) = 0;

		bool isBasic() {
			return (type == number || type == word || type == boolean);
		}

		std::string getTypeName() {
			switch (type) {
			case number:
				return "number";
			case word:
				return "word";
			case boolean:
				return "boolean";
			case sentence:
				return "sentence";
			case list:
				return "list";
			case array:
				return "array";
			case list_with_single_value:
			default:
				assert(0);
			}
		}

		//friend std::ostream& operator<<(std::ostream&, LogoType*);
	};

	class LogoBasicDataType :public LogoType
	{
	private:
		double number;
		std::string word;
		bool boolean;
		
	public:
		LogoBasicDataType(int value);
		LogoBasicDataType(double value);
		LogoBasicDataType(const char* value);
		LogoBasicDataType(bool value);

		//cp
		/*LogoBasicDataType(LogoBasicDataType& p) {
			this->setType(p.getType());

			switch (this->type)
			{
			case DataTypes::number:
				this->number = p.getNumberValue();
				break;
			case DataTypes::word:
				this->word = p.getWordValue();
				break;
			case DataTypes::boolean:
				this->boolean = p.getBooleanValue();
				break;
			case list_with_single_value:
			default:
				assert(0);
			}
		}
		*/
	
		void setNumberValue(double value) {
			this->number = value;
		}

		void setWordValue(std::string value) {
			this->word = value;
		}

		void setBooleanValue(bool value) {
			this->boolean = value;
		}

		void setType(DataTypes type) {
			assert(type != DataTypes::list);
			assert(type != DataTypes::array);
			assert(type != DataTypes::sentence);
			this->type = type;
		}

		double getNumberValue() {
			return this->number;
		}

		std::string getWordValue() {
			return this->word;
		}

		bool getBooleanValue() {
			return this->boolean;
		}

		operator bool() const {
			return false;
		}

		LogoBasicDataType();
		~LogoBasicDataType();
		
		LogoBasicDataType operator-(); //unary
		friend std::ostream& operator<<(std::ostream&, LogoBasicDataType p);
		//friend std::ostream& operator<<(std::ostream&, LogoBasicDataType* p);



	};

	class LogoComplexDataType : public LogoType {
	public:
		bool append_list_to_me;

		LogoComplexDataType();
		~LogoComplexDataType();

		//cp
		/*
		LogoComplexDataType(LogoComplexDataType& p)	{
			switch (p.getType()) {
				case array:
				case sentence:
				case list:
					this->internal_list = p.internal_list;
					break;
				default:
					assert(0);
			}
			this->type = p.getType();
		}*/
		
		LogoComplexDataType(LogoBasicDataType& p);
		LogoComplexDataType(std::initializer_list<LogoComplexDataType> l);

		void setType(DataTypes type) {
			assert(type != DataTypes::number);
			assert(type != DataTypes::boolean);
			assert(type != DataTypes::word);

			//do we allow unset as a type?
			this->type = type;
		}

		LogoComplexDataType operator[](LogoComplexDataType p);
		LogoComplexDataType operator[](LogoBasicDataType p);

		std::vector<ListEntry*> getList() {
			return internal_list;
		}

		void addToList(LogoBasicDataType p);

		void addToList(LogoComplexDataType p);

		friend std::ostream& operator<<(std::ostream&, LogoComplexDataType p);
		friend std::ostream& operator<<(std::ostream&, LogoComplexDataType* p);

		friend LogoComplexDataType operator,(LogoBasicDataType p1, LogoComplexDataType p2);
		friend LogoComplexDataType operator,(LogoComplexDataType p1, LogoComplexDataType p2);
		friend LogoComplexDataType operator,(LogoComplexDataType p1, LogoBasicDataType p2);
		friend LogoComplexDataType operator,(LogoComplexDataType p1, std::initializer_list<LogoComplexDataType> l);

		operator bool() const {
			return true;
		}

		std::vector<ListEntry*>::iterator begin()
		{
			return internal_list.begin();
		}

		std::vector<ListEntry*>::iterator end()
		{
			return internal_list.end();
		}
		
	public:
		std::vector<ListEntry*> internal_list;
	};


	class ListEntry : public LogoType {

	private:
		LogoBasicDataType* lb;
		LogoComplexDataType* lc;
		//DataTypes type;

	public:
		ListEntry(LogoBasicDataType lb) {
			this->lb = new LogoBasicDataType(lb);
			type = lb.getType();
		}

		ListEntry(LogoComplexDataType lc) {
			this->lc = new LogoComplexDataType();
			this->lc->internal_list = lc.internal_list;
			this->lc->setType(lc.getType());
			type = lc.getType();
		}

		LogoBasicDataType* getBasicDataType() {
			return lb;
		}

		LogoComplexDataType* getComplexDataType() {
			return lc;
		}

		DataTypes getListEntryType() {
			return type;
		}

		void setBasicDataType(LogoBasicDataType* lb) {
			this->lb = lb;
		}

		void setComplexDataType(LogoComplexDataType* lc) {
			this->lc = lc;
		}

		void setBasicDataType(LogoBasicDataType lb) {
			this->lb = new LogoBasicDataType(lb);
		}

		void setComplexDataType(LogoComplexDataType lc) {
			this->lc = new LogoComplexDataType(lc);
		}

		void setType(DataTypes type) {}

		friend std::ostream& operator<<(std::ostream&, ListEntry&);
		friend std::ostream& operator<<(std::ostream&, ListEntry*);
	};

	LogoBasicDataType operator/(LogoBasicDataType p1, LogoBasicDataType p2);
	LogoBasicDataType operator%(LogoBasicDataType p1, LogoBasicDataType p2);
	LogoBasicDataType operator-(LogoBasicDataType p1, LogoBasicDataType p2);
	bool operator==(LogoBasicDataType p1, LogoBasicDataType p2);
	bool operator!=(LogoBasicDataType p1, LogoBasicDataType p2);
	bool operator>(LogoBasicDataType p1, LogoBasicDataType p2);
	bool operator>=(LogoBasicDataType p1, LogoBasicDataType p2);
	bool operator<(LogoBasicDataType p1, LogoBasicDataType p2);
	bool operator<=(LogoBasicDataType p1, LogoBasicDataType p2);

	bool operator==(LogoComplexDataType p1, LogoBasicDataType p2);
	bool operator==(LogoComplexDataType p1, LogoComplexDataType p2);

	LogoComplexDataType operator,(LogoBasicDataType p1, LogoBasicDataType p2);
	LogoComplexDataType getElemValue(LogoComplexDataType index,LogoComplexDataType arr);
	LogoComplexDataType getElemValue(LogoComplexDataType index, ListEntry* arr);
	void setElemValue(LogoComplexDataType index, LogoComplexDataType arr,LogoComplexDataType newValue);
	void setElemValue(LogoComplexDataType index, LogoComplexDataType arr, LogoBasicDataType newValue);

	template<typename ...Types>
	LogoBasicDataType sum(Types... rest) {
		TRACE_INFO("IN sum");
		LogoBasicDataType values[] = {rest... };
		LogoBasicDataType ret;

		int sum = 0;
		for (LogoBasicDataType v : values)
			sum += v.getNumberValue();

		ret.setType(logo::DataTypes::number);
		ret.setNumberValue(sum);
		return ret;
	}

	template<typename ...Types>
	LogoBasicDataType product(Types... rest) {
		TRACE_INFO("IN product");
		LogoBasicDataType values[] = {rest... };
		LogoBasicDataType ret;

		int product = 1;
		for (LogoBasicDataType v : values)
			product *= v.getNumberValue();

		ret.setType(logo::DataTypes::number);
		ret.setNumberValue(product);
		return ret;
	}

	template<typename T, typename ...Types>
	LogoComplexDataType get_sentence(T p, Types... rest) {
		TRACE_INFO("IN get_sentence");
		LogoBasicDataType values[] = { p,rest... };
		LogoComplexDataType ret;

		for (auto v : values)
			ret.internal_list.push_back(new ListEntry(v));

		ret.setType(DataTypes::sentence);

		for (auto n : ret.getList())
			TRACE_INFO(n);

		return ret;
	}

	template<typename ...Types>
	bool and_(Types... rest) {
		bool values[] = {rest... };

		bool ret = true;

		for (auto n : values)
			ret = ret && n;
		return ret;
	}

	template<typename ...Types>
	bool or_(Types... rest) {
		bool values[] = {rest... };

		bool ret = false;

		for (auto n : values)
			ret = ret || n;
		return ret;
	}

	
}

