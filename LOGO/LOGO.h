#pragma once
#include "DataTypes.h"
#include <vector>
#include <stack>

#include "hy352_gui.h"
#include "turtle_control.h"

int control::x = 0;
int control::y = 0;
bool control::are_coords = false;
bool control::x_set = false;
bool control::y_set = false;

std::stack<int> $_loop_stack;

control::forward $_f;
control::backward $_b;
control::right $_r;
control::left $_l;
control::circle $_c;
control::pen_size $_p;
control::pen_color $_p_color;
control::screen_color $_s_color;
control::print_on_canvas $_p_label;

control::Show $_show;

logo::LogoComplexDataType $_list_holder;
logo::LogoBasicDataType $p;

#define START_PROGRAM	int main(void) {		\
						init_GUI();

#define END_PROGRAM		;system("PAUSE");destroy_GUI();return 0; }

#define MAKE			;auto

#define NUMBER			$p = false ? -1
#define WORD			$p = false ? "err"
#define BOOLEAN			$p = false ? false

#define FALSE			false
#define TRUE			true

#define SENTENCE(...)	logo::get_sentence(__VA_ARGS__)
#define LIST			$_list_holder
#define ARRAY			logo::LogoComplexDataType

#define ITEM(X,...)		logo::getElemValue(logo::LogoComplexDataType X, __VA_ARGS__)
#define SETITEM(X,...)  ;logo::setElemValue(logo::LogoComplexDataType X,__VA_ARGS__)

#define SUM(...)		logo::sum(__VA_ARGS__) 
#define PRODUCT(...)	logo::product(__VA_ARGS__)
#define QUOTIENT(X,Y)	(X / Y)
#define MODULO(X,Y)		(X % Y)
#define DIFFERENCE(X,Y) (X - Y)
#define MINUS(X)		-( X )

#define AND(X,...)		logo::and_(X,__VA_ARGS__)
#define OR(...)			logo::or_(__VA_ARGS__)

#define TIMES			).getNumberValue() ); $_loop_stack.top() > 0; $_loop_stack.top()--

#define WHILE			 1)); true == false; ); $_loop_stack.pop();while (

#define IF				;if(
#define ELSE			;}else{

#define	REPCOUNT		$_loop_stack.top()

#define	REPEAT			;for($_loop_stack.push( (

#define ASSIGN			;

#define FORWARD			;$_f =
#define BACK			;$_b =
#define RIGHT			;$_r =
#define LEFT			;$_l =
#define	CENTER			;turtle_go_to_center();
#define CIRCLE			;$_c = 

#define PENUP			;pen_up();
#define PENDOWN			;pen_down();
#define SETPENSIZE		;$_p = 
#define ELEM			n
#define FOREACH			;for(logo::ListEntry* n :

#define TO				;void
#define RETURN			;return;
#define FEND			;}
#define WITH			(logo::LogoComplexDataType

#define ELIF			;}else if(
#define ARG(X)			logo::getElemValue((X),args);
#define SHOW			;$_show = false ? $_show
#define CALL			;

#define SETXY			;control::are_coords = true;logo::LogoBasicDataType 
#define SETPENCOLOR		;$_p_color = 
#define SETSCREENCOLOR	;$_s_color = 	
#define PRINT			;$_p_label = 

#define END				;} \
						if (!$_loop_stack.empty() && $_loop_stack.top() == 0) { \
								$_loop_stack.pop(); \
						}

#define DO				){
#define FSTART			){