#include "pch.h"

#include "DataTypes.h"
#include "turtle_control.h"
using namespace logo;

void check_if_coords(double value) 
{
	if (control::are_coords && !control::y_set)
	{
		control::y = value;
		control::y_set = true;
	}
	else if (control::are_coords && !control::x_set)
	{
		control::x = value;
		std::cout << "Moving to turtle to " << control::x << " " << control::y << std::endl;
		control::x_set = true;

		turtle_go_to_position(control::x, control::y);
		control::are_coords = false;
	}

	if (control::y_set && control::x_set)
	{
		control::y_set = false;
		control::x_set = false;

	}
}

LogoBasicDataType::LogoBasicDataType(int value)
{
	check_if_coords(value);
	TRACE_INFO("New LogoBasicDataType with int value: " << value);
	this->number = value;
	this->type = DataTypes::number;
}

LogoBasicDataType::LogoBasicDataType(double value)
{
	check_if_coords(value);
	TRACE_INFO("New LogoBasicDataType with int value: " << value);
	this->number = value;
	this->type = DataTypes::number;

}

LogoBasicDataType::LogoBasicDataType(const char* value)
{
	TRACE_INFO("New LogoBasicDataType with const char* value: " << value);
	std::string s(value);
	this->word = s;
	this->type = DataTypes::word;
}

LogoBasicDataType::LogoBasicDataType()
{
	TRACE_INFO("Base LogoBasicDataType");
	this->type = DataTypes::list_with_single_value; // if left unset, it will trigger an assertion
}

LogoBasicDataType::LogoBasicDataType(bool value)
{
	TRACE_INFO("New LogoBasicDataType with bool value: " << value);
	this->boolean = value;
	this->type = DataTypes::boolean;
}

LogoBasicDataType::~LogoBasicDataType()
{
}

std::ostream& logo::operator<<(std::ostream& os, LogoBasicDataType p)
{
	switch (p.getType())
	{
	case DataTypes::number:
		os << " " << p.getNumberValue();
		break;
	case DataTypes::word:
		os << " " << p.getWordValue().c_str();
		break;
	case DataTypes::boolean:
		if (p.getBooleanValue())
			os << " TRUE";
		else
			os << " FALSE";
		break;
	case list_with_single_value:
	default:
		assert(0);
	}
	return os;
}

LogoComplexDataType::LogoComplexDataType()
{
	this->append_list_to_me = false;
}

LogoComplexDataType::~LogoComplexDataType()
{
}


LogoComplexDataType::LogoComplexDataType(LogoBasicDataType& p) {
	this->internal_list.push_back(new ListEntry(p));
	this->setType(DataTypes::list_with_single_value); //unset means not a list,array,sentence so
									// it contains only one basic data type.
}
//TODO: test empty array,empty list....

LogoComplexDataType::LogoComplexDataType(std::initializer_list<LogoComplexDataType> l)
{
	TRACE_INFO("ARRAY FOUND");
	TRACE_INFO(l.size());
	for (auto n : l) {
		ListEntry* toadd;
		switch (n.getType()) {
		case number:
		case boolean:
		case word:
			toadd = new ListEntry(n);
			this->internal_list.push_back(toadd);		
			break;
		case list:
		case array:
		case sentence:
			toadd = new ListEntry(n);
			this->internal_list.push_back(toadd);
			break;
		case list_with_single_value: //unroll
			if (n.internal_list[0]->isBasic())
				toadd = new ListEntry(*(n.internal_list[0]->getBasicDataType()));
			else
				toadd = new ListEntry(*(n.internal_list[0]->getComplexDataType()));

			this->internal_list.push_back(toadd);
			break;
		default:
			//fix for empty arrays!!
			assert(0);
		}
		
	}
	this->setType(DataTypes::array);
	this->append_list_to_me = false;
}

void LogoComplexDataType::addToList(LogoBasicDataType p) {
	this->internal_list.push_back(new ListEntry(p));
	this->setType(DataTypes::list_with_single_value);
}

void LogoComplexDataType::addToList(LogoComplexDataType p) {
	for (auto n : p.internal_list)
		this->internal_list.push_back(n);
	this->setType(DataTypes::list_with_single_value);
}

std::ostream& logo::operator<<(std::ostream& os, LogoComplexDataType p)
{
	switch (p.getType())
	{
	case array:
		os << " {";
		for (auto n : p.internal_list)
			std::cout << *n << " ";
		os << "} ";
		break;
	case sentence:
		os << " (";
		for (auto n : p.internal_list)
			os << *n << " ";
		os << ") ";
		break;
	case list:
		os << " [";
		for (auto n : p.internal_list)
			std::cout << *n << " ";
		os << "] ";
		break;
	case list_with_single_value:
		if(p.internal_list[0]->isBasic())
			std::cout << *(p.internal_list[0]->getBasicDataType()) << " ";
		else
			std::cout << *(p.internal_list[0]->getComplexDataType()) << " ";
		break;
	default:
		assert(0);
	}

	return os;
}

std::ostream& logo::operator<<(std::ostream& os, LogoComplexDataType* p)
{
	assert(0);
	return os << *p;
}
LogoComplexDataType LogoComplexDataType::operator[](LogoComplexDataType p) {
	p.setType(DataTypes::list);
	//INFO("$_list_holder: " << p);
	p.append_list_to_me = false;
	return p;
}

LogoComplexDataType LogoComplexDataType::operator[](LogoBasicDataType p) {
	LogoComplexDataType ret;
	ret.setType(DataTypes::list);
	TRACE_INFO("$_list_holder: " << ret);
	ret.append_list_to_me = false;
	return ret;
}

std::ostream& logo::operator<<(std::ostream& os, ListEntry& e)
{
	switch (e.getListEntryType())
	{
	case number:
	case boolean:
	case word:
		os << *(e.getBasicDataType());
		break;
	case list:
	case array:
	case list_with_single_value:
		//INFO("list with single value!");
	case sentence:
		os << *(e.getComplexDataType());
		break;
	default:
		assert(0);
	}
	return os;
}

std::ostream& logo::operator<<(std::ostream& os, ListEntry* e)
{
	switch (e->getListEntryType())
	{
	case number:
	case boolean:
	case word:
		os << *(e->getBasicDataType());
		break;
	case list:
	case array:
	case list_with_single_value:
		//INFO("list with single value!");
	case sentence:
		os << *(e->getComplexDataType());
		break;
	default:
		assert(0);
	}
	return os;
}


LogoComplexDataType logo::operator,(LogoBasicDataType p1, LogoBasicDataType p2)
{
	TRACE_INFO("LogoBasicDataType comma LogoBasicDataType: " << p1 << " " << p2);
	LogoComplexDataType new_complex;
	new_complex.setType(DataTypes::list_with_single_value);
	new_complex.internal_list.push_back(new ListEntry(p1));
	new_complex.internal_list.push_back(new ListEntry(p2));
	new_complex.append_list_to_me = true;
	return new_complex;
}

LogoComplexDataType logo::operator,(LogoBasicDataType p1, LogoComplexDataType p2)
{
	TRACE_INFO("LogoBasicDataType comma LogoComplexDataType: " << p1 << " " << p2);
	LogoComplexDataType new_complex;
	new_complex.setType(DataTypes::list_with_single_value);
	new_complex.internal_list.push_back(new ListEntry(p1));
	new_complex.internal_list.push_back(new ListEntry(p2));
	new_complex.append_list_to_me = true;
	return new_complex;
}

LogoComplexDataType logo::operator,(LogoComplexDataType p1, LogoComplexDataType p2)
{
	TRACE_INFO("LogoComplexDataType comma LogoComplexDataType: " << p1 << " " << p2);
	
	if (p1.append_list_to_me) {
		p1.internal_list.push_back(new ListEntry(p2));
		return p1;
	}

	LogoComplexDataType new_complex;
	new_complex.setType(DataTypes::list);
	new_complex.internal_list.push_back(new ListEntry(p1));
	new_complex.internal_list.push_back(new ListEntry(p2));

	new_complex.append_list_to_me = true;
	return new_complex;

}

LogoComplexDataType logo::operator,(LogoComplexDataType p1, LogoBasicDataType p2)
{
	TRACE_INFO("LogoComplexDataType comma LogoBasicDataType: " << p1 << " " << p2);
	p1.internal_list.push_back(new ListEntry(p2));
	p1.append_list_to_me = true;
	return p1;
}

ListEntry* getListEntry(LogoComplexDataType index, LogoComplexDataType arr)
{
	LogoComplexDataType next = arr;
	ListEntry* ret = NULL;
	for (auto n : index.internal_list) {
		int i = (int)n->getBasicDataType()->getNumberValue() - 1;

		switch (next.internal_list[i]->getType()) {
		case word:
		case number:
		case boolean:
			return next.internal_list[i];
		case sentence:
		case list:
		case DataTypes::array:
			ret = next.internal_list[i];
			next = *(next.internal_list[i]->getComplexDataType());
			break;
		default:
			assert(0);
		}

	}
	return ret;
}

LogoComplexDataType logo::getElemValue(LogoComplexDataType index,LogoComplexDataType arr)
{
	ListEntry* target = getListEntry(index, arr);
	switch (target->getType()) {
	case word:
	case number: 
	case boolean: 
		return *(target->getBasicDataType());
	case sentence:
	case list:
	case array:
		return *(target->getComplexDataType());
	default:
		assert(0);
	}	
}

LogoComplexDataType logo::getElemValue(LogoComplexDataType index, ListEntry* arr)
{
	return getElemValue(index, *(arr->getComplexDataType()));
}


void logo::setElemValue(LogoComplexDataType index, LogoComplexDataType arr, LogoComplexDataType newValue)
{
	ListEntry* target = getListEntry(index, arr);
	switch (newValue.getType()) {
		target->setBasicDataType(NULL);
		target->setComplexDataType(NULL);
		target->setType(newValue.getType());
	case word:
	case number:
	case boolean:
		assert(0);
		break;
	case sentence:
	case list:
	case array:
		target->setComplexDataType(newValue);
		break;
	default:
		assert(0);
	}
}

void logo::setElemValue(LogoComplexDataType index, LogoComplexDataType arr, LogoBasicDataType newValue)
{
	ListEntry* target = getListEntry(index, arr);
	switch (target->getType()) {
		target->setBasicDataType(NULL);
		target->setComplexDataType(NULL);
		target->setType(newValue.getType());
	case word:
	case number:
	case boolean:
		target->setBasicDataType(newValue);
		break;
	case sentence:
	case list:
	case array:
		assert(0);
		break;
	default:
		assert(0);
	}
}

LogoBasicDataType logo::operator/(LogoBasicDataType p1, LogoBasicDataType p2)
{
	assert(p1.getType() == logo::DataTypes::number &&
			p2.getType() == logo::DataTypes::number &&
			"Illegal operands for '/' !"
	);

	LogoBasicDataType p = LogoBasicDataType(p1.getNumberValue() / p2.getNumberValue());
	p.setType(logo::DataTypes::number);
	return p;
}

LogoBasicDataType logo::operator%(LogoBasicDataType p1, LogoBasicDataType p2)
{
	assert(p1.getType() == logo::DataTypes::number &&
		p2.getType() == logo::DataTypes::number &&
		"Illegal operands for '%' !"
	);

	LogoBasicDataType p = LogoBasicDataType(fmod(p1.getNumberValue(),p2.getNumberValue()));
	p.setType(logo::DataTypes::number);
	return p;
}



LogoBasicDataType logo::operator-(LogoBasicDataType p1, LogoBasicDataType p2)
{
	assert(p1.getType() == logo::DataTypes::number &&
		p2.getType() == logo::DataTypes::number &&
		"Illegal operands for '-' !"
	);

	LogoBasicDataType p = LogoBasicDataType(p1.getNumberValue() - p2.getNumberValue());
	p.setType(logo::DataTypes::number);
	return p;
}

LogoBasicDataType logo::LogoBasicDataType::operator-()
{
	assert(this->getType() == logo::DataTypes::number &&
		"Illegal operands for '-' (unary) !"
	);

	LogoBasicDataType p = LogoBasicDataType(- this->getNumberValue());
	p.setType(logo::DataTypes::number);
	return p;
}

bool logo::operator==(LogoBasicDataType p1, LogoBasicDataType p2)
{
	if (p1.getType() != p2.getType()) {
		std::cout << "Warning:Operands for equality operator '==' must have the same type!Found " << p1.getTypeName() << " and " << p2.getTypeName() << std::endl;
		return false;
	}
	
	switch (p1.getType())
	{
	case number:
		return p1.getNumberValue() == p2.getNumberValue();
	case word:
		return p1.getWordValue() == p2.getWordValue();
	case boolean:
		return p1.getBooleanValue() == p2.getBooleanValue();
	default:
		assert(0);
	}

	return false; //compiler warning...
}

bool logo::operator==(LogoComplexDataType p1, LogoBasicDataType p2)
{
	if ( p1.getType() != logo::DataTypes::list_with_single_value ||
		p1.internal_list[0]->getBasicDataType()->getType() != p2.getType()) {
		std::cout << "Warning: Operands for equality operator '==' must have the same type!Found " << p1.internal_list[0]->getBasicDataType()->getTypeName() << " and " << p2.getTypeName() << std::endl;
		return false;
	}

	switch (p1.internal_list[0]->getBasicDataType()->getType())
	{
	case number:
		return p1.internal_list[0]->getBasicDataType()->getNumberValue() == p2.getNumberValue();
	case word:
		return p1.internal_list[0]->getBasicDataType()->getWordValue() == p2.getWordValue();
	case boolean:
		return p1.internal_list[0]->getBasicDataType()->getBooleanValue() == p2.getBooleanValue();
	default:
		assert(0);
	}

	return false; //compiler...
}

bool logo::operator==(LogoComplexDataType p1, LogoComplexDataType p2)
{
	if (p1.getType() != p2.getType()) {
		std::cout << "Warning: Operands for equality operator '==' must have the same type!Found " << p1.getTypeName() << " and " << p2.getTypeName() << std::endl;
		return false;
	}

	if (p1.internal_list.size() != p2.internal_list.size())
		return false;
	
	for (int i = 0; i < p1.internal_list.size(); i++) {
		if (p1.internal_list[i]->getType() != p1.internal_list[i]->getType())
			return false;
		else if (p1.internal_list[i]->isBasic())
			return p1.internal_list[i]->getBasicDataType() == p2.internal_list[i]->getBasicDataType();
		else
			return p1.internal_list[i]->getComplexDataType() == p2.internal_list[i]->getComplexDataType();
	}
	return true;
}

bool logo::operator!=(LogoBasicDataType p1, LogoBasicDataType p2)
{
	return !(p1 == p2);
}

bool logo::operator>(LogoBasicDataType p1, LogoBasicDataType p2)
{
	if (p1.getType() != p2.getType()) {
		std::cout << "Error:Operands for greater than operator '>' must have the same type!Found " << p1.getTypeName() << " and " << p2.getTypeName() << std::endl;
	}

	switch (p1.getType())
	{
	case number:
		return p1.getNumberValue() > p2.getNumberValue();
	case word:
		return p1.getWordValue() > p2.getWordValue();
	case boolean:
		return p1.getBooleanValue() > p2.getBooleanValue();
	default:
		std::cout << "Error:Operands for greater than operator '>' must be numbers,words or boolean!Found " << p1.getTypeName() << " and " << p2.getTypeName() << std::endl;
		assert(0);
	}

	return false; //...

}

bool logo::operator>=(LogoBasicDataType p1, LogoBasicDataType p2)
{
	return (p1 > p2 || p1 == p2);
}

bool logo::operator<(LogoBasicDataType p1, LogoBasicDataType p2)
{
	return !(p1 > p2 || p1 == p2);
}

bool logo::operator<=(LogoBasicDataType p1, LogoBasicDataType p2)
{
	return (p1 < p2 || p1 == p2);
}

