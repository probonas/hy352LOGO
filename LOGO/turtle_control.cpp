#include "turtle_control.h"
#include <string>

control::Show::Show(logo::ListEntry* toShow)
{
	std::cout << "SHOW says:" << toShow << std::endl;
}

control::Show::Show(int toShow)
{
	std::cout << "SHOW says:" << toShow << std::endl;
}

control::Show::Show(float toShow)
{
	std::cout << "SHOW says:" << toShow << std::endl;
}

control::Show::Show(bool toShow)
{
	std::cout << "SHOW says:" << toShow << std::endl;
}

control::Show::Show(std::string toShow)
{
	std::cout << "SHOW says:" << toShow << std::endl;
}

control::Show::Show(logo::LogoBasicDataType toShow)
{
	std::cout << "SHOW says:" << toShow << std::endl;
}

control::Show::Show(logo::LogoComplexDataType toShow)
{
	std::cout << "SHOW says:" << toShow << std::endl;
}

control::forward::forward(logo::LogoBasicDataType steps)
{
	INFO("Forward " << steps.getNumberValue() << " steps.");
	turtle_mv_forward(steps.getNumberValue());
}

control::forward::forward(logo::LogoComplexDataType steps)
{
	assert(steps.getType() == logo::DataTypes::list_with_single_value
		&& "Calling FORWARD with a list is not supported.");
	forward(*(steps.internal_list[0]->getBasicDataType()));
}


control::backward::backward(logo::LogoBasicDataType steps)
{
	INFO("Backward " << steps.getNumberValue() << " steps.");
	turtle_mv_backward(steps.getNumberValue());
}

control::backward::backward(logo::LogoComplexDataType steps)
{
	assert(steps.getType() == logo::DataTypes::list_with_single_value
		&& "Calling BACKWARD with a list is not supported.");

	backward(*(steps.internal_list[0]->getBasicDataType()));
}

control::left::left(logo::LogoBasicDataType degrees)
{
	INFO("Left " << degrees.getNumberValue() << " degress.");
	turtle_rotate(-degrees.getNumberValue());
}

control::left::left(logo::LogoComplexDataType degrees)
{
	assert(degrees.getType() == logo::DataTypes::list_with_single_value
		&& "Calling LEFT with a list is not supported.");

	left(*(degrees.internal_list[0]->getBasicDataType()));
}

control::right::right(logo::LogoBasicDataType degrees)
{
	INFO("Right " << degrees.getNumberValue() << " degress.");
	turtle_rotate(degrees.getNumberValue());
}

control::right::right(logo::LogoComplexDataType degrees)
{
	assert(degrees.getType() == logo::DataTypes::list_with_single_value
		&& "Calling RIGHT with a list is not supported.");

	right(*(degrees.internal_list[0]->getBasicDataType()));
}

control::circle::circle(logo::LogoBasicDataType radius)
{
	INFO("Moving on circle with " << radius.getNumberValue() << " radius.");
	int r = radius.getNumberValue();
	turtle_draw_circle(r);
	float center_x, center_y;
	center_x = turtle_x;
	center_y = turtle_y;
	
	//pen_up();
	
	for (int i = 0; i < 360; i+=20) { //move 20 pixels at a time
		float n_x = cos(degreesToRadians(i))*r + center_x;
		float n_y = sin(degreesToRadians(i))*r + center_y;
		turtle_go_to_position(n_x,n_y);
		INFO("Turtle moved to (" << n_x << "," << n_y << ")");
	}
	
	//pen_down();
}

control::circle::circle(logo::LogoComplexDataType radius)
{
	assert(radius.getType() == logo::DataTypes::list_with_single_value
		&& "Calling CIRCLE with a list is not supported.");

	circle(*(radius.internal_list[0]->getBasicDataType()));
}


control::pen_size::pen_size(logo::LogoBasicDataType thickness)
{
	INFO("Pensize is ", thickness.getNumberValue()," pixels thick.")
	set_pen_thickness(thickness.getNumberValue());
}

control::pen_size::pen_size(logo::LogoComplexDataType thickness)
{
	assert(thickness.getType() == logo::DataTypes::list_with_single_value
		&& "Calling PENSIZE with a list is not supported.");

	pen_size(*(thickness.internal_list[0]->getBasicDataType()));
}

control::screen_color::screen_color(logo::LogoComplexDataType color_list)
{
	INFO("Changed screen color to " << color_list);
	set_screen_color(color_list.internal_list[0]->getBasicDataType()->getNumberValue(),
		color_list.internal_list[1]->getBasicDataType()->getNumberValue(),
		color_list.internal_list[2]->getBasicDataType()->getNumberValue());
}

control::pen_color::pen_color(logo::LogoComplexDataType color_list)
{
	INFO("Changed pen color to " << color_list);
	set_pen_color(color_list.internal_list[0]->getBasicDataType()->getNumberValue(),
		color_list.internal_list[1]->getBasicDataType()->getNumberValue(),
		color_list.internal_list[2]->getBasicDataType()->getNumberValue());

}

control::print_on_canvas::print_on_canvas(logo::LogoBasicDataType msg)
{
	turtle_draw_label(msg.getWordValue().c_str());
}


