#define BOOST_TEST_MODULE mytests
#include <boost/test/included/unit_test.hpp>
#include "LOGO.h"

BOOST_AUTO_TEST_CASE(TestNumber)
{
	MAKE num = (NUMBER: 30);
	BOOST_TEST(num.getType() == logo::DataTypes::number);
	BOOST_REQUIRE_EQUAL((int)num.getNumberValue(), 30);
}

BOOST_AUTO_TEST_CASE(TestWord)
{

	MAKE word = (WORD: "TESTwORD");
	BOOST_TEST(word.getType() == logo::DataTypes::word);
	BOOST_REQUIRE_EQUAL(strcmp(word.getWordValue().c_str(), "TESTwORD"), 0);
}

BOOST_AUTO_TEST_CASE(TestSentence)
{

	MAKE s = SENTENCE((WORD : "THIS"), (WORD : "IS"), (WORD:"A"), (WORD:"TEST"), (WORD:"SENTENCE"));
	BOOST_TEST(s.getType() == logo::DataTypes::sentence);
	int size = s.internal_list.size();
	BOOST_REQUIRE_EQUAL(size, 5);
	BOOST_TEST(s.internal_list[0]->getBasicDataType()->getType() == logo::DataTypes::word);
	BOOST_REQUIRE_EQUAL(strcmp(s.internal_list[0]->getBasicDataType()->getWordValue().c_str(), "THIS"), 0);
	BOOST_TEST(s.internal_list[1]->getBasicDataType()->getType() == logo::DataTypes::word);
	BOOST_REQUIRE_EQUAL(strcmp(s.internal_list[1]->getBasicDataType()->getWordValue().c_str(), "IS"), 0);
	BOOST_TEST(s.internal_list[2]->getBasicDataType()->getType() == logo::DataTypes::word);
	BOOST_REQUIRE_EQUAL(strcmp(s.internal_list[2]->getBasicDataType()->getWordValue().c_str(), "A"), 0);
	BOOST_TEST(s.internal_list[3]->getBasicDataType()->getType() == logo::DataTypes::word);
	BOOST_REQUIRE_EQUAL(strcmp(s.internal_list[3]->getBasicDataType()->getWordValue().c_str(), "TEST"), 0);
	BOOST_TEST(s.internal_list[4]->getBasicDataType()->getType() == logo::DataTypes::word);
	BOOST_REQUIRE_EQUAL(strcmp(s.internal_list[4]->getBasicDataType()->getWordValue().c_str(), "SENTENCE"), 0);
}

BOOST_AUTO_TEST_CASE(TestBoolean)
{


	MAKE b = (BOOLEAN: TRUE);
	BOOST_TEST(b.getType() == logo::DataTypes::boolean);
	BOOST_TEST(b.getBooleanValue() == true);

	MAKE c = (BOOLEAN: FALSE);
	BOOST_TEST(c.getType() == logo::DataTypes::boolean);
	BOOST_TEST(c.getBooleanValue() == false);
}

BOOST_AUTO_TEST_CASE(TestList)
{
	;
	MAKE l = LIST[(WORD : "$%DFVSD\n"), (NUMBER:13), (NUMBER:21), (BOOLEAN:TRUE), (WORD:""), (BOOLEAN:FALSE)];

	int size = l.internal_list.size();

	BOOST_REQUIRE_EQUAL(size, 6);
	BOOST_TEST(l.getType() == logo::DataTypes::list);

	BOOST_TEST(l.internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[1]->getType() == logo::DataTypes::number);
	BOOST_TEST(l.internal_list[2]->getType() == logo::DataTypes::number);
	BOOST_TEST(l.internal_list[3]->getType() == logo::DataTypes::boolean);
	BOOST_TEST(l.internal_list[4]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[5]->getType() == logo::DataTypes::boolean);

	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[0]->getBasicDataType()->getWordValue().c_str(), "$%DFVSD\n"), 0);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[1]->getBasicDataType()->getNumberValue(), 13);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[2]->getBasicDataType()->getNumberValue(), 21);
	BOOST_REQUIRE_EQUAL(l.internal_list[3]->getBasicDataType()->getBooleanValue(), true);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[4]->getBasicDataType()->getWordValue().c_str(), ""), 0);
	BOOST_REQUIRE_EQUAL(l.internal_list[5]->getBasicDataType()->getBooleanValue(), false);

}

BOOST_AUTO_TEST_CASE(ListWithBasicDataTypesAndList)
{
	;
	MAKE l = LIST[
		(WORD : "O GIANNIS"), (NUMBER:13),
			LIST[(WORD:"EVALE EDW ENA STRING"), (NUMBER:21)],
			(BOOLEAN:TRUE)
	];

	int size = l.internal_list.size();
	BOOST_REQUIRE_EQUAL(size, 4);

	BOOST_TEST(l.internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[1]->getType() == logo::DataTypes::number);
	BOOST_TEST(l.internal_list[2]->getType() == logo::DataTypes::list);
	BOOST_TEST(l.internal_list[3]->getType() == logo::DataTypes::boolean);

	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[0]->getBasicDataType()->getWordValue().c_str(), "O GIANNIS"), 0);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[1]->getBasicDataType()->getNumberValue(), 13);

	int list_in_list_size = l.internal_list[2]->getComplexDataType()->internal_list.size();
	BOOST_REQUIRE_EQUAL(list_in_list_size, 2);

	BOOST_TEST(l.internal_list[2]->getType() == logo::DataTypes::list);
	BOOST_TEST(l.internal_list[2]->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[2]->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);

	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[2]->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "EVALE EDW ENA STRING"), 0);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[2]->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 21);

	BOOST_REQUIRE_EQUAL(l.internal_list[3]->getBasicDataType()->getBooleanValue(), true);

}

BOOST_AUTO_TEST_CASE(ListWithLists)
{
	;
	MAKE l = LIST[
		LIST[(WORD:"TEST"), (NUMBER:21)],
			LIST[(WORD:"TEST"), (NUMBER:21)],
			LIST[(WORD:"TEST"), (NUMBER:21)],
			LIST[(WORD:"TEST"), (NUMBER:21)],
			LIST[(WORD:"TEST"), (NUMBER:21)]
	];

	BOOST_TEST(l.getType() == logo::DataTypes::list);
	std::cout << l;
	for (auto n : l.internal_list) {
		BOOST_TEST(n->getType() == logo::DataTypes::list);
		BOOST_TEST(n->getComplexDataType()->getType() == logo::DataTypes::list);

		BOOST_TEST(n->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
		BOOST_TEST(n->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);

		BOOST_REQUIRE_EQUAL(strcmp(n->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "TEST"), 0);
		BOOST_REQUIRE_EQUAL((int)n->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 21);

	}
}

BOOST_AUTO_TEST_CASE(ListWithArray)
{
	;
	MAKE l = LIST[
		ARRAY{ (WORD:"FORWARD"), (NUMBER:100) },
			ARRAY{ (WORD:"RIGHT"), (NUMBER:90) },
			ARRAY{ (WORD:"FORWARD"), (NUMBER:100) },
			ARRAY{ (WORD:"RIGHT"), (NUMBER:90) },
			ARRAY{ (WORD:"FORWARD"), (NUMBER:100) },
			ARRAY{ (WORD:"RIGHT"), (NUMBER:90) },
			ARRAY{ (WORD:"FORWARD"), (NUMBER:100) },
			ARRAY{ (WORD:"RIGHT"), (NUMBER:90) }
	];

	int size = l.internal_list.size();
	BOOST_REQUIRE_EQUAL(size, 8);

	BOOST_TEST(l.getType() == logo::DataTypes::list);
	int i = 0;

	for (auto n : l.internal_list) {
		BOOST_TEST(n->getComplexDataType()->getType() == logo::DataTypes::array);
		BOOST_TEST(n->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
		BOOST_TEST(n->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);
		if (i % 2 == 0) {
			BOOST_REQUIRE_EQUAL(strcmp(n->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "FORWARD"), 0);
			BOOST_REQUIRE_EQUAL((int)n->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 100);
		}
		else {
			BOOST_REQUIRE_EQUAL(strcmp(n->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "RIGHT"), 0);
			BOOST_REQUIRE_EQUAL((int)n->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 90);
		}
		i++;
	}



}

BOOST_AUTO_TEST_CASE(ListMixed)
{
	;
	MAKE l = LIST[
		(NUMBER:13),
			ARRAY{ (WORD:"FORWARD"), (NUMBER:100), LIST[(WORD:"RIGHT"), (NUMBER:90)] },
			SENTENCE((WORD:"S1"), (WORD:"S2"), (WORD:"S3"), (WORD:"S4"), (WORD:"S5")),
			LIST[(WORD:"FORWARD"), (NUMBER:100)],
			LIST[(WORD:"RIGHT"), (NUMBER:90)],
			(WORD:"W1"),
			LIST[(WORD:"FORWARD"), (NUMBER:100)],
			LIST[(WORD:"RIGHT"), (NUMBER:90)],
			(BOOLEAN:TRUE)
	];

	int size = l.internal_list.size();
	BOOST_REQUIRE_EQUAL(size, 9);
	BOOST_TEST(l.getType() == logo::DataTypes::list);

	BOOST_TEST(l.internal_list[0]->getType() == logo::DataTypes::number);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[0]->getBasicDataType()->getNumberValue(), 13);

	BOOST_TEST(l.internal_list[1]->getType() == logo::DataTypes::array);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[1]->getComplexDataType()->internal_list.size(), 3);

	BOOST_TEST(l.internal_list[1]->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[1]->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);
	BOOST_TEST(l.internal_list[1]->getComplexDataType()->internal_list[2]->getType() == logo::DataTypes::list);

	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[1]->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "FORWARD"), 0);

	BOOST_REQUIRE_EQUAL((int)l.internal_list[1]->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 100);

	BOOST_REQUIRE_EQUAL((int)l.internal_list[1]->getComplexDataType()->internal_list[2]->getComplexDataType()->internal_list.size(), 2);

	BOOST_TEST(l.internal_list[1]->getComplexDataType()->internal_list[2]->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[1]->getComplexDataType()->internal_list[2]->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[1]->getComplexDataType()->internal_list[2]->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "RIGHT"), 0);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[1]->getComplexDataType()->internal_list[2]->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 90);

	BOOST_TEST(l.internal_list[2]->getType() == logo::DataTypes::sentence);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[2]->getComplexDataType()->internal_list.size(), 5);
	BOOST_TEST(l.internal_list[2]->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[2]->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[2]->getComplexDataType()->internal_list[2]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[2]->getComplexDataType()->internal_list[3]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[2]->getComplexDataType()->internal_list[4]->getType() == logo::DataTypes::word);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[2]->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "S1"), 0);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[2]->getComplexDataType()->internal_list[1]->getBasicDataType()->getWordValue().c_str(), "S2"), 0);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[2]->getComplexDataType()->internal_list[2]->getBasicDataType()->getWordValue().c_str(), "S3"), 0);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[2]->getComplexDataType()->internal_list[3]->getBasicDataType()->getWordValue().c_str(), "S4"), 0);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[2]->getComplexDataType()->internal_list[4]->getBasicDataType()->getWordValue().c_str(), "S5"), 0);

	BOOST_TEST(l.internal_list[3]->getType() == logo::DataTypes::list);
	BOOST_TEST(l.internal_list[3]->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[3]->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[3]->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "FORWARD"), 0);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[3]->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 100);

	BOOST_TEST(l.internal_list[4]->getType() == logo::DataTypes::list);
	BOOST_TEST(l.internal_list[4]->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[4]->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[4]->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "RIGHT"), 0);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[4]->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 90);

	BOOST_TEST(l.internal_list[5]->getType() == logo::DataTypes::word);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[5]->getBasicDataType()->getWordValue().c_str(), "W1"), 0);

	BOOST_TEST(l.internal_list[6]->getType() == logo::DataTypes::list);
	BOOST_TEST(l.internal_list[6]->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[6]->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[6]->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "FORWARD"), 0);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[6]->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 100);

	BOOST_TEST(l.internal_list[7]->getType() == logo::DataTypes::list);
	BOOST_TEST(l.internal_list[7]->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[7]->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[7]->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "RIGHT"), 0);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[7]->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 90);

	BOOST_TEST(l.internal_list[8]->getType() == logo::DataTypes::boolean);
	BOOST_REQUIRE_EQUAL(l.internal_list[8]->getBasicDataType()->getBooleanValue(), true);

}

BOOST_AUTO_TEST_CASE(Simple)
{
	;
	MAKE l = ARRAY{ (WORD : "$%DFVSD\n"), (NUMBER:13), (NUMBER:21), (BOOLEAN:TRUE), (WORD:""), (BOOLEAN:FALSE) };

	int size = l.internal_list.size();

	BOOST_REQUIRE_EQUAL(size, 6);
	BOOST_TEST(l.getType() == logo::DataTypes::array);

	BOOST_TEST(l.internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[1]->getType() == logo::DataTypes::number);
	BOOST_TEST(l.internal_list[2]->getType() == logo::DataTypes::number);
	BOOST_TEST(l.internal_list[3]->getType() == logo::DataTypes::boolean);
	BOOST_TEST(l.internal_list[4]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[5]->getType() == logo::DataTypes::boolean);

	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[0]->getBasicDataType()->getWordValue().c_str(), "$%DFVSD\n"), 0);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[1]->getBasicDataType()->getNumberValue(), 13);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[2]->getBasicDataType()->getNumberValue(), 21);
	BOOST_REQUIRE_EQUAL(l.internal_list[3]->getBasicDataType()->getBooleanValue(), true);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[4]->getBasicDataType()->getWordValue().c_str(), ""), 0);
	BOOST_REQUIRE_EQUAL(l.internal_list[5]->getBasicDataType()->getBooleanValue(), false);

}

BOOST_AUTO_TEST_CASE(ArrayWithArray)
{
	;
	MAKE l = ARRAY{
		ARRAY{ (WORD:"FORWARD"), (NUMBER:100) },
		ARRAY{ (WORD:"RIGHT"), (NUMBER:90) },
		ARRAY{ (WORD:"FORWARD"), (NUMBER:100) },
		ARRAY{ (WORD:"RIGHT"), (NUMBER:90) },
		ARRAY{ (WORD:"FORWARD"), (NUMBER:100) },
		ARRAY{ (WORD:"RIGHT"), (NUMBER:90) },
		ARRAY{ (WORD:"FORWARD"), (NUMBER:100) },
		ARRAY{ (WORD:"RIGHT"), (NUMBER:90) }
	};

	int size = l.internal_list.size();
	BOOST_REQUIRE_EQUAL(size, 8);

	BOOST_TEST(l.getType() == logo::DataTypes::array);
	int i = 0;

	for (auto n : l.internal_list) {
		BOOST_TEST(n->getComplexDataType()->getType() == logo::DataTypes::array);
		BOOST_TEST(n->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
		BOOST_TEST(n->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);
		if (i % 2 == 0) {
			BOOST_REQUIRE_EQUAL(strcmp(n->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "FORWARD"), 0);
			BOOST_REQUIRE_EQUAL((int)n->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 100);
		}
		else {
			BOOST_REQUIRE_EQUAL(strcmp(n->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "RIGHT"), 0);
			BOOST_REQUIRE_EQUAL((int)n->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 90);
		}
		i++;
	}
}

BOOST_AUTO_TEST_CASE(ArrayWithList)
{
	;
	MAKE l = ARRAY{
		LIST[(WORD:"FORWARD"), (NUMBER:100)],
		LIST[(WORD:"RIGHT"), (NUMBER:90)],
		LIST[(WORD:"FORWARD"), (NUMBER:100)],
		LIST[(WORD:"RIGHT"), (NUMBER:90)],
		LIST[(WORD:"FORWARD"), (NUMBER:100)],
		LIST[(WORD:"RIGHT"), (NUMBER:90)],
		LIST[(WORD:"FORWARD"), (NUMBER:100)],
		LIST[(WORD:"RIGHT"), (NUMBER:90)]
	};

	int size = l.internal_list.size();
	BOOST_REQUIRE_EQUAL(size, 8);

	BOOST_TEST(l.getType() == logo::DataTypes::array);
	int i = 0;

	for (auto n : l.internal_list) {
		BOOST_TEST(n->getComplexDataType()->getType() == logo::DataTypes::list);
		BOOST_TEST(n->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
		BOOST_TEST(n->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);
		if (i % 2 == 0) {
			BOOST_REQUIRE_EQUAL(strcmp(n->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "FORWARD"), 0);
			BOOST_REQUIRE_EQUAL((int)n->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 100);
		}
		else {
			BOOST_REQUIRE_EQUAL(strcmp(n->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "RIGHT"), 0);
			BOOST_REQUIRE_EQUAL((int)n->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 90);
		}
		i++;
	}
}

BOOST_AUTO_TEST_CASE(ArrayMixed)
{
	;
	MAKE l = ARRAY{
		(NUMBER:13),
		ARRAY{ (WORD:"FORWARD"), (NUMBER:100), LIST[(WORD:"RIGHT"), (NUMBER:90)]},
		SENTENCE((WORD:"S1"),(WORD:"S2"),(WORD:"S3"),(WORD:"S4"),(WORD:"S5")),
		LIST[(WORD:"FORWARD"), (NUMBER:100)],
		LIST[(WORD:"RIGHT"), (NUMBER:90)],
		(WORD:"W1"),
		LIST[(WORD:"FORWARD"), (NUMBER:100)],
		LIST[(WORD:"RIGHT"), (NUMBER:90)],
		(BOOLEAN:TRUE)
	};

	int size = l.internal_list.size();
	BOOST_REQUIRE_EQUAL(size, 9);
	BOOST_TEST(l.getType() == logo::DataTypes::array);

	BOOST_TEST(l.internal_list[0]->getType() == logo::DataTypes::number);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[0]->getBasicDataType()->getNumberValue(), 13);

	BOOST_TEST(l.internal_list[1]->getType() == logo::DataTypes::array);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[1]->getComplexDataType()->internal_list.size(), 3);

	BOOST_TEST(l.internal_list[1]->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[1]->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);
	BOOST_TEST(l.internal_list[1]->getComplexDataType()->internal_list[2]->getType() == logo::DataTypes::list);

	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[1]->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "FORWARD"), 0);

	BOOST_REQUIRE_EQUAL((int)l.internal_list[1]->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 100);

	BOOST_REQUIRE_EQUAL((int)l.internal_list[1]->getComplexDataType()->internal_list[2]->getComplexDataType()->internal_list.size(), 2);

	BOOST_TEST(l.internal_list[1]->getComplexDataType()->internal_list[2]->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[1]->getComplexDataType()->internal_list[2]->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[1]->getComplexDataType()->internal_list[2]->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "RIGHT"), 0);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[1]->getComplexDataType()->internal_list[2]->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 90);

	BOOST_TEST(l.internal_list[2]->getType() == logo::DataTypes::sentence);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[2]->getComplexDataType()->internal_list.size(), 5);
	BOOST_TEST(l.internal_list[2]->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[2]->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[2]->getComplexDataType()->internal_list[2]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[2]->getComplexDataType()->internal_list[3]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[2]->getComplexDataType()->internal_list[4]->getType() == logo::DataTypes::word);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[2]->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "S1"), 0);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[2]->getComplexDataType()->internal_list[1]->getBasicDataType()->getWordValue().c_str(), "S2"), 0);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[2]->getComplexDataType()->internal_list[2]->getBasicDataType()->getWordValue().c_str(), "S3"), 0);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[2]->getComplexDataType()->internal_list[3]->getBasicDataType()->getWordValue().c_str(), "S4"), 0);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[2]->getComplexDataType()->internal_list[4]->getBasicDataType()->getWordValue().c_str(), "S5"), 0);

	BOOST_TEST(l.internal_list[3]->getType() == logo::DataTypes::list);
	BOOST_TEST(l.internal_list[3]->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[3]->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[3]->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "FORWARD"), 0);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[3]->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 100);

	BOOST_TEST(l.internal_list[4]->getType() == logo::DataTypes::list);
	BOOST_TEST(l.internal_list[4]->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[4]->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[4]->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "RIGHT"), 0);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[4]->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 90);

	BOOST_TEST(l.internal_list[5]->getType() == logo::DataTypes::word);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[5]->getBasicDataType()->getWordValue().c_str(), "W1"), 0);

	BOOST_TEST(l.internal_list[6]->getType() == logo::DataTypes::list);
	BOOST_TEST(l.internal_list[6]->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[6]->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[6]->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "FORWARD"), 0);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[6]->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 100);

	BOOST_TEST(l.internal_list[7]->getType() == logo::DataTypes::list);
	BOOST_TEST(l.internal_list[7]->getComplexDataType()->internal_list[0]->getType() == logo::DataTypes::word);
	BOOST_TEST(l.internal_list[7]->getComplexDataType()->internal_list[1]->getType() == logo::DataTypes::number);
	BOOST_REQUIRE_EQUAL(strcmp(l.internal_list[7]->getComplexDataType()->internal_list[0]->getBasicDataType()->getWordValue().c_str(), "RIGHT"), 0);
	BOOST_REQUIRE_EQUAL((int)l.internal_list[7]->getComplexDataType()->internal_list[1]->getBasicDataType()->getNumberValue(), 90);

	BOOST_TEST(l.internal_list[8]->getType() == logo::DataTypes::boolean);
	BOOST_REQUIRE_EQUAL(l.internal_list[8]->getBasicDataType()->getBooleanValue(), true);
}

BOOST_AUTO_TEST_CASE(LogicalAnd)
{
	;
	MAKE number = (NUMBER:25);
	MAKE text = (WORD:"TEST");
	BOOST_TEST(AND(number == (NUMBER:25), text == ("TEST")));
}

BOOST_AUTO_TEST_CASE(LogicalAndFail)
{
	;
	MAKE number = (NUMBER:25);
	MAKE text = (WORD:"TEST");
	BOOST_TEST(AND(number == (NUMBER:25), text == ("TEST_fail")) == false);
}

BOOST_AUTO_TEST_CASE(LogicalOr)
{
	MAKE number = (NUMBER: 21)
	MAKE text = (WORD: "hello")
	MAKE text2 = (WORD: "sadly_no_hello")
	MAKE myMoves = LIST [ LIST [(WORD: "FORWARD"), (NUMBER: 100)], LIST [(WORD: "LEFT"), (NUMBER: 90)], LIST [(WORD: "FORWARD"), (NUMBER: 100)] ] 
	MAKE array = ARRAY{ number, text, (NUMBER: 12), (BOOLEAN: TRUE), ARRAY { myMoves, LIST[(WORD: "BACK"), (NUMBER:100)] } };

	BOOST_TEST( OR ( number == (NUMBER : 5), ITEM({ (NUMBER:3) }, array), text2 == text));
}

BOOST_AUTO_TEST_CASE(LogicalOrFail)
{
	MAKE number = (NUMBER: 21)
	MAKE text = (WORD: "hello")
	MAKE text2 = (WORD: "sadly_no_hello")
	MAKE myMoves = LIST[LIST[(WORD:"FORWARD"), (NUMBER:100)], LIST[(WORD:"LEFT"), (NUMBER:90)], LIST[(WORD:"FORWARD"), (NUMBER:100)]]
	MAKE array = ARRAY{ number, text, (NUMBER: 12), (BOOLEAN: TRUE), ARRAY { myMoves, LIST[(WORD: "BACK"), (NUMBER:100)] } };

	BOOST_TEST(OR(number == (NUMBER : 6), text2 == text) == false);
}

BOOST_AUTO_TEST_CASE(SetItem)
{
	MAKE number = (NUMBER: 21)
	MAKE text = (WORD: "hello")
	MAKE text2 = (WORD: "sadly_no_hello")
	MAKE myMoves = LIST[LIST[(WORD:"FORWARD"), (NUMBER:100)], LIST[(WORD:"LEFT"), (NUMBER:90)], LIST[(WORD:"FORWARD"), (NUMBER:100)]]
	MAKE array = ARRAY{ number, text, (NUMBER: 12), (BOOLEAN: TRUE), ARRAY { myMoves, LIST[(WORD: "BACK"), (NUMBER:100)] } };

	SETITEM({ (NUMBER:3) }, array, (NUMBER: 50));
	BOOST_TEST(ITEM({ (NUMBER:3) },array) == (NUMBER:50));
}