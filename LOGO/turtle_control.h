#pragma once

#include "DataTypes.h"
#include "hy352_gui.h"

#define INFO(X) std::cout << X << std::endl;

namespace control {
	class Show {
	public:
		Show(logo::ListEntry* toShow);
		Show(int		toShow);
		Show(float		toShow);
		Show(bool		toShow);
		Show(std::string toShow);
		Show(logo::LogoBasicDataType toShow);
		Show(logo::LogoComplexDataType toShow);

		Show() {};
	};

	class forward {
	public:
		forward(logo::LogoBasicDataType steps);
		forward(logo::LogoComplexDataType steps);
		forward() {}
	};

	class backward {
	public:
		backward(logo::LogoBasicDataType steps);
		backward(logo::LogoComplexDataType steps);
		backward() {}
	};

	class left {
	public:
		left(logo::LogoBasicDataType degrees);
		left(logo::LogoComplexDataType degrees);
		left() {}
	};

	class right {
	public:
		right(logo::LogoBasicDataType degrees);
		right(logo::LogoComplexDataType degrees);
		right() {}
	};

	class circle {
	public:
		circle(logo::LogoBasicDataType radius);
		circle(logo::LogoComplexDataType radius);
		circle(){}
	};

	class pen_size {
	public:
		pen_size(logo::LogoBasicDataType thickness);
		pen_size(logo::LogoComplexDataType thickness);
		pen_size() {};
	};

	class screen_color {
	public:
		screen_color(logo::LogoComplexDataType color_list);
		screen_color(){}
	};

	class pen_color {
	public:
		pen_color(logo::LogoComplexDataType color_list);
		pen_color() {}
	};

	class print_on_canvas {
	public:
		print_on_canvas(logo::LogoBasicDataType msg);
		print_on_canvas(){}
	};

	extern int x;
	extern int y;
	extern bool are_coords;
	extern bool x_set;
	extern bool y_set;

}

