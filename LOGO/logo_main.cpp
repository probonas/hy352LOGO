// LOGO.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include "LOGO.h"
#include <iostream>

TO draw_square WITH args FSTART // args is a list [length, [r,g,b]]
	SETPENCOLOR ARG(NUMBER: 2)
	REPEAT(NUMBER:4) TIMES
	DO
		FORWARD ARG(NUMBER: 1)
		RIGHT(NUMBER: 90)
	END
FEND

/*
MAKE num = (NUMBER: 30)
MAKE w = (WORD: "THIS IS WORD")
MAKE b = (BOOLEAN: TRUE)

MAKE s = SENTENCE((WORD : "ELA"), (WORD : "EDW"), (WORD:"RE"), (WORD:"POUSTH"))


MAKE l = LIST[
		(WORD : "O GIANNIS"), (NUMBER:13),
		LIST[(WORD:"EVALE EDW ENA STRING"), (NUMBER:21)],
		(BOOLEAN:TRUE)
		]

MAKE l2 = LIST[
			LIST[(WORD:"TEST"), (NUMBER:21)],
			LIST[(WORD:"TEST"), (NUMBER:22)]
	];

MAKE f = ARRAY{
		(WORD : "O GIANNIS"), (NUMBER:13),
		LIST[(WORD:"EVALE EDW ENA STRING"), (NUMBER:21)],
		(BOOLEAN:TRUE)
		}
*/
START_PROGRAM

MAKE turtleMoves = LIST[
		ARRAY{ (WORD:"FORWARD"), (NUMBER:100) },
		ARRAY{ (WORD:"RIGHT"), (NUMBER:90) },
		ARRAY{ (WORD:"FORWARD"), (NUMBER:100) },
		ARRAY{ (WORD:"RIGHT"), (NUMBER:90) },
		ARRAY{ (WORD:"FORWARD"), (NUMBER:100) },
		ARRAY{ (WORD:"LEFT"), (NUMBER:90) },
		ARRAY{ (WORD:"FORWARD"), (NUMBER:100) },
		ARRAY{ (WORD:"LEFT"), (NUMBER:90) }
]

SETPENCOLOR LIST[(NUMBER:0), (NUMBER:0), (NUMBER:255)]
SETSCREENCOLOR LIST[(NUMBER:0), (NUMBER:255), (NUMBER:255)]
SETPENSIZE (NUMBER:10)
CENTER
SETXY(NUMBER: 350),(NUMBER:200)
CENTER
REPEAT(NUMBER:36) TIMES DO
	CALL draw_square(LIST[PRODUCT((NUMBER:12), REPCOUNT),
					 LIST[PRODUCT((NUMBER:8), REPCOUNT), (NUMBER:0), (NUMBER:0)]])
	RIGHT(NUMBER: 10)
END

REPEAT (NUMBER:2) TIMES DO
	FOREACH turtleMoves DO
		IF ITEM({ (NUMBER: 1) }, ELEM) == (WORD: "FORWARD") DO
			FORWARD ITEM({ (NUMBER: 2) }, ELEM)
		ELIF ITEM({ (NUMBER: 1) }, ELEM) == (WORD: "BACK") DO
			BACK ITEM({ (NUMBER: 2) }, ELEM)
		ELIF ITEM({ (NUMBER: 1) }, ELEM) == (WORD: "LEFT") DO
			LEFT ITEM({ (NUMBER: 2) }, ELEM)
		ELSE
			RIGHT ITEM({ (NUMBER: 2) }, ELEM)
		END
	END
END

PENUP
CENTER
PRINT(WORD:"HELLO MY FRIEND")
RIGHT(NUMBER: 90)
FORWARD(NUMBER: 150)
PENDOWN

REPEAT(NUMBER: 10) TIMES DO
	SHOW: REPCOUNT
	FORWARD(NUMBER: 3)
	RIGHT(NUMBER: 1)
END

CENTER
CIRCLE (NUMBER:50)

PENDOWN

REPEAT(NUMBER: 360) TIMES DO
	FORWARD(NUMBER: 2)
	RIGHT(NUMBER: 1)
END

SETPENSIZE (NUMBER:10)
REPEAT (NUMBER : 5) TIMES DO

FORWARD (NUMBER:100)
RIGHT	(NUMBER:20)
END
CIRCLE (NUMBER:50)
FOREACH turtleMoves DO
SHOW: ELEM
PRINT (WORD:"TEST")
END
PRINT(WORD:"THAT'S ALL FOLKS")

END_PROGRAM


/*
MAKE number = (NUMBER: 100)
MAKE hello = ( WORD: "hello")

MAKE array = ARRAY{
	number,
	hello,
	(NUMBER:12),
	(BOOLEAN: TRUE),
	ARRAY{
		turtleMoves,
		LIST[(WORD:"YES!"),(NUMBER:100)]
	}
}

;std::cout << "******** RESULTS ********" << std::endl;
; std::cout << w << std::endl;
; std::cout << b << std::endl;
; std::cout << s << std::endl;
; std::cout << l << std::endl;
; std::cout << f << std::endl;

MAKE m = ITEM({ (NUMBER:5),(NUMBER:2) }, array)
; std::cout << ">>>>>>" << m << std::endl;
SETITEM({ (NUMBER:2) }, array, (WORD:"CHANGED!"));
m = ITEM({ (NUMBER:2)}, array)
; std::cout << ">>>>>>" << m << std::endl;
; MAKE sum = SUM((NUMBER:1), (NUMBER:2), (NUMBER:3), (NUMBER:4), (NUMBER:5), (NUMBER:6), (NUMBER:7));
; std::cout << ">>>>>>" << sum << std::endl;
MAKE prod = PRODUCT((NUMBER:1), (NUMBER:2), (NUMBER:3), (NUMBER:4), (NUMBER:5), (NUMBER:6), (NUMBER:7));
//; std::cout << sum((1), (2), (3));
; std::cout << ">>>>>>" << prod << std::endl;


IF num == (NUMBER:30) DO
std::cout << "OK BITCH" << std::endl;
END

IF num == (NUMBER:35) DO
std::cout << "NOT OK BITCH" << std::endl;
END


REPEAT NUMBER : 5 TIMES
DO
std::cout << "OK..." << std::endl;
REPEAT NUMBER : 2 TIMES
DO
std::cout << " i " << $_loop_stack.top() << std::endl;
IF num == (NUMBER : 30) DO
std::cout << "yes" << std::endl;
		END
	END
END

REPEAT WHILE num == (NUMBER:30) DO
std::cout << "fine" << std::endl;
ASSIGN num = SUM((NUMBER:35),num)
END
*/